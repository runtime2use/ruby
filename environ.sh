#!/bin/bash

export RUBY_PATH="$LANG_PATH/dist"

#export GEM_PATH="$LANG_PATH/dist"

################################################################################

ronin_require_ruby () {
    echo hello world >/dev/null
}

ronin_include_ruby () {
    motd_text "    -> Ruby    : "$RUBY_PATH
}

################################################################################

ronin_setup_ruby () {
    for cmd in foreman god ; do
        if [[ ! -f /usr/bin/$cmd ]] ; then
            gem install --no-rdoc --no-ri $cmd
        fi
    done
}

